#include "FLOConnectionInstanceDAO.h"

#include <QDebug>

FLOConnectionInstanceDAO::FLOConnectionInstanceDAO(QObject *parent) : FLODAO (parent)
{

}

FLOConnectionInstanceDAO::FLOConnectionInstanceDAO(QUuid uuid, QUuid outputModuleUuid, int outputModulePortNumber, QUuid inputModuleUuid, int inputModulePortNumber) : FLODAO (),
    m_uuid(uuid), m_outputModuleUuid(outputModuleUuid), m_inputModuleUuid(inputModuleUuid), m_iOutputModulePortNumber(outputModulePortNumber), m_iInputModulePortNumber(inputModulePortNumber){

}

void FLOConnectionInstanceDAO::serialize(oscpkt::Message *message)
{
    message->pushStr(m_uuid.toString().toStdString());
    message->pushStr(m_outputModuleUuid.toString().toStdString());
    message->pushInt32(m_iOutputModulePortNumber);
    message->pushStr(m_inputModuleUuid.toString().toStdString());
    message->pushInt32(m_iInputModulePortNumber);
}

void FLOConnectionInstanceDAO::serialize(QXmlStreamWriter *xmlWriter)
{
    xmlWriter->writeStartElement("Connection");
    xmlWriter->writeAttribute("uuid", m_uuid.toString());
    xmlWriter->writeStartElement("OutputModule");
    xmlWriter->writeAttribute( "uuid", m_outputModuleUuid.toString());
    xmlWriter->writeAttribute("portNumber", QString::number(m_iOutputModulePortNumber));
    xmlWriter->writeEndElement();
    xmlWriter->writeStartElement("InputModule");
    xmlWriter->writeAttribute("uuid", m_inputModuleUuid.toString());
    xmlWriter->writeAttribute("portNumber", QString::number(m_iInputModulePortNumber));
    xmlWriter->writeEndElement();
    xmlWriter->writeEndElement();
}

void FLOConnectionInstanceDAO::deserialize(oscpkt::Message *message)
{
    QString uuid, outputModuleUuid, inputModuleUuid;
    oscpkt::Message::ArgReader argReader = message->arg().popStr(uuid).popStr(outputModuleUuid).popInt32(m_iOutputModulePortNumber)
                                           .popStr(inputModuleUuid).popInt32(m_iInputModulePortNumber);
    m_uuid = uuid;
    m_outputModuleUuid = outputModuleUuid;
    m_inputModuleUuid = inputModuleUuid;
}

void FLOConnectionInstanceDAO::deserialize(QXmlStreamReader *xmlReader)
{
    QXmlStreamReader::TokenType t = xmlReader->tokenType();
    QStringRef s = xmlReader->name();
    QString text;
    while(!xmlReader->atEnd())
    {
        switch( t )
        {
            case QXmlStreamReader::TokenType::StartElement:
                s = xmlReader->name();
                text.clear();
                qDebug() << "Model: Element Name: " << s;
                if( s == "Connection")
                {
                    QXmlStreamAttributes attributes = xmlReader->attributes();
                    for( auto attribute : attributes )
                    {
                        QStringRef name = attribute.name();
                        if( name == "uuid")
                            setUuid(attribute.value().toString());
                        qDebug() << "\tAttribute Name: " << name << ", value: " << attribute.value();
                    }
                }
                if( s == "OutputModule" )
                {
                    QXmlStreamAttributes attributes = xmlReader->attributes();
                    for( auto attribute : attributes )
                    {
                        QStringRef name = attribute.name();
                        qDebug() << "\tAttribute Name: " << name;
                        if(name == "uuid")
                            setOutputModuleUuid(attribute.value().toString());
                        if(name == "portNumber")
                            setOutputModulePortNumber(attribute.value().toInt());
                    }

                }
                if( s == "InputModule" )
                {
                    QXmlStreamAttributes attributes = xmlReader->attributes();
                    for( auto attribute : attributes )
                    {
                        QStringRef name = attribute.name();
                        qDebug() << "\tAttribute Name: " << name;
                        if(name == "uuid")
                            setInputModuleUuid(attribute.value().toString());
                        if(name == "portNumber")
                            setInputModulePortNumber(attribute.value().toInt());
                    }

                }
                break;
            case QXmlStreamReader::TokenType::Characters:
                {
                    text += xmlReader->text().toString();
                    break;
                }
            case QXmlStreamReader::TokenType::EndElement:
                s = xmlReader->name();
                break;
            default:
                break;
        }
        t = xmlReader->readNext();
    }

}

QUuid FLOConnectionInstanceDAO::getOutputModuleUuid() const
{
    return m_outputModuleUuid;
}

void FLOConnectionInstanceDAO::setOutputModuleUuid(const QUuid &value)
{
    m_outputModuleUuid = value;
}

QUuid FLOConnectionInstanceDAO::getInputModuleUuid() const
{
    return m_inputModuleUuid;
}

void FLOConnectionInstanceDAO::setInputModuleUuid(const QUuid &value)
{
    m_inputModuleUuid = value;
}

int FLOConnectionInstanceDAO::getOutputModulePortNumber() const
{
    return m_iOutputModulePortNumber;
}

void FLOConnectionInstanceDAO::setOutputModulePortNumber(int value)
{
    m_iOutputModulePortNumber = value;
}

int FLOConnectionInstanceDAO::getInputModulePortNumber() const
{
    return m_iInputModulePortNumber;
}

void FLOConnectionInstanceDAO::setInputModulePortNumber(int value)
{
    m_iInputModulePortNumber = value;
}

QUuid FLOConnectionInstanceDAO::getUuid() const
{
    return m_uuid;
}

void FLOConnectionInstanceDAO::setUuid(const QUuid &uuid)
{
    m_uuid = uuid;
}
