#ifndef FLOCONNECTIONINSTANCEDAO_H
#define FLOCONNECTIONINSTANCEDAO_H

#include <QObject>
#include <QUuid>

#include "FLODAO.h"

class FLOConnectionInstanceDAO : public FLODAO
{
public:
    explicit FLOConnectionInstanceDAO(QObject *parent = nullptr);
    explicit FLOConnectionInstanceDAO(QUuid m_uuid, QUuid m_outputModuleUuid, int m_iOutputModulePortNumber,
                                      QUuid m_inputModuleUuid, int m_iInputModulePortNumber);


    // FLODAO interface
public:
    void serialize(oscpkt::Message *message);
    void serialize(QXmlStreamWriter *xmlWriter);
    void deserialize(oscpkt::Message *message);
    void deserialize(QXmlStreamReader *xmlReader);

    // getter
    QUuid getOutputModuleUuid() const;
    QUuid getInputModuleUuid() const;
    int getOutputModulePortNumber() const;
    int getInputModulePortNumber() const;

    // setter
    void setOutputModuleUuid(const QUuid &value);
    void setInputModuleUuid(const QUuid &value);
    void setOutputModulePortNumber(int value);
    void setInputModulePortNumber(int value);

    QUuid getUuid() const;
    void setUuid(const QUuid &uuid);

private:
    QUuid m_uuid;
    QUuid m_outputModuleUuid;
    QUuid m_inputModuleUuid;

    int m_iOutputModulePortNumber = 0;
    int m_iInputModulePortNumber = 0;

};

#endif // FLOCONNECTIONINSTANCEDAO_H
