#ifndef FLDAO_H
#define FLDAO_H

#include "oscpkt.hh"

#include <QObject>
#include <QXmlStreamWriter>
#include <QXmlStreamReader>


class FLODAO : public QObject{
public:

    FLODAO( QObject *parent = nullptr) : QObject(parent){
    }
    //methods serialize to message
    virtual void serialize(oscpkt::Message *message) = 0;

    //methods serialize to xml
    virtual void serialize(QXmlStreamWriter *xmlWriter) = 0;

    //methods deserialize from message
    virtual void deserialize(oscpkt::Message *message) = 0;

    //methods deserialize from xml
    virtual void deserialize(QXmlStreamReader *xmlReader) = 0;

};

#endif // FLDAO_H
