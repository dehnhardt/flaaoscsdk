SOURCES += \
    $$PWD/src/FLOConnectionInstanceDAO.cpp \
    $$PWD/src/FLOModuleInstanceDAO.cpp \
    $$PWD/src/FLOParameter.cpp \
    $$PWD/src/oschandler.cpp \
    $$PWD/src/osclistener.cpp \
    $$PWD/src/oscsender.cpp

HEADERS += \ \
    $$PWD/src/FLOConnectionInstanceDAO.h \
    $$PWD/src/FLODAO.h \
    $$PWD/src/FLOModuleInstanceDAO.h \
    $$PWD/src/FLOParameter.h \
    $$PWD/src/oschandler.h \
    $$PWD/src/osclistener.h \
    $$PWD/src/oscpkt.hh \
    $$PWD/src/oscsender.h \
    $$PWD/src/udp.hh

DISTFILES += \
    $$PWD/LICENSE \
    $$PWD/README.md

INCLUDEPATH += $$PWD/../flaaoscsdk/src
